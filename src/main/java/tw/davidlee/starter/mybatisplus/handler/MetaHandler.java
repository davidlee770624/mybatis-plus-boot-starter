package tw.davidlee.starter.mybatisplus.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;


@Slf4j
@Component
public class MetaHandler implements MetaObjectHandler {

    /**
     * insert時 , 自動填入功能
     * */
    @Override
    public void insertFill(MetaObject metaObject) {
        // create_Time欄位
        this.strictInsertFill(metaObject, "createTime", java.time.ZonedDateTime.class , ZonedDateTime.now());
        this.fillStrategy(metaObject, "createTime", ZonedDateTime.now());

        // update_Time欄位
        this.strictUpdateFill(metaObject, "updateTime", java.time.ZonedDateTime.class, ZonedDateTime.now());
        this.fillStrategy(metaObject, "updateTime", ZonedDateTime.now());
    }

    /**
     * update時 , 自動填入功能
     * */
    @Override
    public void updateFill(MetaObject metaObject) {
        // update_Time欄位
        this.strictUpdateFill(metaObject, "updateTime", java.time.ZonedDateTime.class, ZonedDateTime.now());
        this.fillStrategy(metaObject, "updateTime", ZonedDateTime.now());
    }
}

