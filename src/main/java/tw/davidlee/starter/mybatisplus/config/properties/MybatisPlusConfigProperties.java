package tw.davidlee.starter.mybatisplus.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "tw.davidlee.mybatisplus" )
@Data
public class MybatisPlusConfigProperties {
    /**
     * 測試YAML提示
     * */
    private String test;
}
